package com.selfie.traveller.application.dagger;


import android.content.Context;

import com.squareup.picasso.Picasso;
import com.selfie.traveller.application.dagger.modules.AppModule;
import com.selfie.traveller.application.dagger.modules.GsonModule;
import com.selfie.traveller.application.dagger.modules.NetworkModule;
import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;


import dagger.Component;
import okhttp3.OkHttpClient;

@AppScope
@Component(modules = {AppModule.class, GsonModule.class, NetworkModule.class})
public interface AppComponent {

    Context context();

    OkHttpClient okHttpClient();

    AppNetwork vepNetwork();


    PreferencesManager preferencesManager();



    Picasso picasso();

}
