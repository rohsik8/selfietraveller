package com.selfie.traveller.ui.activities.login.mvp;


import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.login.LoginParams;
import com.selfie.traveller.ui.activities.login.LoginResponse.LoginResponse;

import io.reactivex.Observable;

public class LoginModel {

    private AppNetwork appNetwork;
    public PreferencesManager preferencesManager;

    public LoginModel(AppNetwork padloktNetwork, PreferencesManager preferencesManager) {
        this.appNetwork = padloktNetwork;
        this.preferencesManager = preferencesManager;
    }
    public Observable<LoginResponse> loginResponseObservable(LoginParams loginParams){
        return appNetwork.loginResponse(loginParams);
    }

}
