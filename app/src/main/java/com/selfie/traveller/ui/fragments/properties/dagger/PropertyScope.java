package com.selfie.traveller.ui.fragments.properties.dagger;


import javax.inject.Scope;

@Scope
public @interface PropertyScope {
}
