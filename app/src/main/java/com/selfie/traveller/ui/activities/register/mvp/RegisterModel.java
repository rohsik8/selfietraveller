package com.selfie.traveller.ui.activities.register.mvp;


import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.register.RegisterParams;
import com.selfie.traveller.ui.activities.register.RegisterResponse.RegisterResponse;

import io.reactivex.Observable;

public class RegisterModel {

    private AppNetwork appNetwork;
    public PreferencesManager preferencesManager;

    public RegisterModel(AppNetwork padloktNetwork, PreferencesManager preferencesManager) {
        this.appNetwork = padloktNetwork;
        this.preferencesManager = preferencesManager;
    }
    public Observable<RegisterResponse> registerResponseObservable(RegisterParams registerParams){
        return appNetwork.registerResponse(registerParams);
    }

}
