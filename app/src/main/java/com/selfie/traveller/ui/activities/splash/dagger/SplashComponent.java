package com.selfie.traveller.ui.activities.splash.dagger;




import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.activities.splash.SplashActivity;

import dagger.Component;

@SplashScope
@Component(modules = SplashModule.class, dependencies = AppComponent.class)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);
}
