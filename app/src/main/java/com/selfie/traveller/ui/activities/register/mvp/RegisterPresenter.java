package com.selfie.traveller.ui.activities.register.mvp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.selfie.traveller.ui.activities.homepage.HomePageActivity;
import com.selfie.traveller.ui.activities.register.RegisterParams;
import com.selfie.traveller.ui.activities.register.RegisterResponse.RegisterResponse;
import com.selfie.traveller.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

import static android.app.Activity.RESULT_CANCELED;
import static com.selfie.traveller.ui.activities.register.dialog.RegisterFifthDialog.REQUEST_TAKE_PHOTO1;
import static com.selfie.traveller.utils.GeneralUtils.getErrorMessage;

public class RegisterPresenter {


    private File actualImage;
    File tempOutputFile;
    File compressedImage;
    ProgressDialog progressDialog;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private RegisterView registerView;
    private RegisterModel registerModel;

    public RegisterPresenter(RegisterView registerView, RegisterModel registerModel) {
        this.registerView = registerView;
        this.registerModel = registerModel;
    }

    public void onCreateView() {

        registerView.registerFifthDialog.binding.btnSignup.setOnClickListener(v -> {
            register();
        });

        registerView.registerFifthDialog.binding.btnPrevious.setOnClickListener(v -> {
            register();
        });
    }

    public void register() {
        //registerView.showLoading(true);
        progressDialog = new ProgressDialog(registerView.mActivity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        DisposableObserver<RegisterResponse> disposableObserver = new DisposableObserver<RegisterResponse>() {
            @Override
            public void onNext(RegisterResponse response) {
                registerModel.preferencesManager.save(Constants.FIRSTNAME,response.getUser().getFirstName());
                registerModel.preferencesManager.save(Constants.USERID,response.getUser().getId()+"");
                registerModel.preferencesManager.save(Constants.LASTNAME,response.getUser().getLastName());
                registerModel.preferencesManager.save(Constants.EMAIL,response.getUser().getEmail());
                registerView.mActivity.startActivity(new Intent(registerView.mActivity, HomePageActivity.class));
                registerView.mActivity.finish();
            }

            @Override
            public void onError(Throwable e) {
                progressDialog.dismiss();
                if (e instanceof HttpException) {
                    ResponseBody responseBody = ((HttpException) e).response().errorBody();
                    registerView.showMessage(getErrorMessage(responseBody));

                } else if (e instanceof SocketTimeoutException) {

                } else if (e instanceof IOException) {
                    registerView.showMessage("Please check your network connection");

                } else {
                    registerView.showMessage(e.getMessage());
                }
            }

            @Override
            public void onComplete() {
                progressDialog.dismiss();
                //registerView.showLoading(false);
            }
        };
        registerModel.registerResponseObservable(registerParams())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }


    public RegisterParams registerParams() {
        return RegisterParams.builder()

                .email(registerView.binding.etEmail.getText().toString().isEmpty()?"":registerView.binding.etEmail.getText().toString())
                .password(registerView.binding.etPassword.getText().toString().isEmpty()?"":registerView.binding.etPassword.getText().toString())
                .passwordConfirmation(registerView.binding.etConfirmPassword.getText().toString().isEmpty()?"":registerView.binding.etConfirmPassword.getText().toString())

                .firstName(registerView.registerSecondDialog.binding.etFirstname.getText().toString().isEmpty()?"":registerView.registerSecondDialog.binding.etFirstname.getText().toString())
                .lastName(registerView.registerSecondDialog.binding.etLastname.getText().toString().isEmpty()?"":registerView.registerSecondDialog.binding.etLastname.getText().toString())
                .phone(registerView.registerSecondDialog.binding.edtMobile.getText().toString().isEmpty()?"":registerView.registerSecondDialog.binding.edtMobile.getText().toString())

                .address(registerView.registerThirdDialog.binding.etAddress.getText().toString().isEmpty()?"":registerView.registerThirdDialog.binding.etAddress.getText().toString())
                .city(registerView.registerThirdDialog.binding.etCity.getText().toString().isEmpty()?"":registerView.registerThirdDialog.binding.etCity.getText().toString())
                .state(registerView.registerThirdDialog.binding.etState.getText().toString().isEmpty()?"":registerView.registerThirdDialog.binding.etState.getText().toString())
                .zip(registerView.registerThirdDialog.binding.etZipcode.getText().toString().isEmpty()?"":registerView.registerThirdDialog.binding.etZipcode.getText().toString())
                .country("Nepal")

                .cardnumber(registerView.registerFourthDialog.binding.etCardno.getText().toString().isEmpty()?"":registerView.registerFourthDialog.binding.etCardno.getText().toString())
                .nameoncard(registerView.registerFourthDialog.binding.etNameOnCard.getText().toString().isEmpty()?"":registerView.registerFourthDialog.binding.etNameOnCard.getText().toString())
                .ccv(registerView.registerFourthDialog.binding.etCcv.getText().toString().isEmpty()?"":registerView.registerFourthDialog.binding.etCcv.getText().toString())
                .expdate(registerView.registerFourthDialog.binding.etExpiryDate.getText().toString().isEmpty()?"":registerView.registerFourthDialog.binding.etExpiryDate.getText().toString()).build();
    }


    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){

        if (resultCode != registerView.mActivity.RESULT_OK||requestCode==RESULT_CANCELED) {
            return;
        }


            //for camera
       if (requestCode == REQUEST_TAKE_PHOTO1 ) {


            try{
                Uri outputFile;

                if (data != null && (data.getAction() == null || !data.getAction().equals(MediaStore.ACTION_IMAGE_CAPTURE))) {
                    outputFile = data.getData();
                    actualImage = new File(getPath(outputFile));

                } else {

                    actualImage = tempOutputFile;
//                actualImageView.setImageBitmap(BitmapFactory.decodeFile(actualImage.getAbsolutePath(),options));
                    // actualSizeTextView.setData(String.format("Size : %s", getReadableFileSize(actualImage.length())));
                }


                if (actualImage == null) {
                    Toast.makeText(registerView.mActivity, "Please Choose Image", Toast.LENGTH_LONG).show();
                } else {

                    registerModel.preferencesManager.save(Constants.PROFILEIMAGE,  actualImage.getAbsolutePath());
                    compressedImage = new Compressor.Builder(registerView.mActivity)
                            .setMaxWidth(1024)
                            .setMaxHeight(720)
                            .setQuality(100)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(registerView.mActivity.getExternalFilesDir(
                                    Environment.DIRECTORY_PICTURES).getAbsolutePath())
                            .build()
                            .compressToFile(actualImage);
                    setPic(requestCode, compressedImage.getAbsolutePath());
                }


            }catch(Exception e){
                Toast.makeText(registerView.mActivity,"CompressedException:"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

    }

    private void setPic(int requestcode, String pathfile) {

        try {

            if (requestcode == 1) {
                // settingView.takeImage1.setImageBitmap(BitmapFactory.decodeFile(picturePath1));
                Picasso.with(registerView.mActivity).load(pathfile)
                        .into(registerView.registerFifthDialog.binding.ivProfile);
            }

        } catch (OutOfMemoryError e) {
            Toast.makeText(registerView.mActivity, "OutOfMemoryException:"+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public String getPath(Uri uri) {
        String s = "";
        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = registerView.mActivity.getContentResolver().query(uri, projection, null, null, null);
            if (cursor == null || uri == null)
                return null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            s = cursor.getString(column_index);
            cursor.close();
        }catch (Exception e){

        }
        return s;
    }


}

