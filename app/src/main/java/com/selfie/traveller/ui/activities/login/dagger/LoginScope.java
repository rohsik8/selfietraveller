package com.selfie.traveller.ui.activities.login.dagger;

import javax.inject.Scope;

@Scope
public @interface LoginScope {
}
