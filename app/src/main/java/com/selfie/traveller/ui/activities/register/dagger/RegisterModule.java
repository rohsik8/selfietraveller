package com.selfie.traveller.ui.activities.register.dagger;

import android.support.v7.app.AppCompatActivity;

import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.register.dialog.RegisterFifthDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterFourthDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterSecondDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterThirdDialog;
import com.selfie.traveller.ui.activities.register.mvp.RegisterModel;
import com.selfie.traveller.ui.activities.register.mvp.RegisterPresenter;
import com.selfie.traveller.ui.activities.register.mvp.RegisterView;

import dagger.Module;
import dagger.Provides;

@Module
public class RegisterModule {

    private final AppCompatActivity activity;

    public RegisterModule(AppCompatActivity activity){
        this.activity = activity;
    }

    @RegisterScope
    @Provides
    public RegisterView loginView(RegisterSecondDialog registerSecondDialog,RegisterThirdDialog registerThirdDialog,RegisterFourthDialog registerFourthDialog,RegisterFifthDialog registerFifthDialog,PreferencesManager preferencesManager){
        return new RegisterView(activity, registerSecondDialog, registerThirdDialog,registerFourthDialog,registerFifthDialog,preferencesManager);
    }

    @RegisterScope
    @Provides
    public RegisterModel loginModel(AppNetwork appNetwork, PreferencesManager preferencesManager){
        return new RegisterModel(appNetwork ,preferencesManager);
    }

    @RegisterScope
    @Provides
    public RegisterPresenter loginPresenter(RegisterView registerView, RegisterModel registerModel){
        return new RegisterPresenter(registerView, registerModel);
    }

    @RegisterScope
    @Provides
    public RegisterSecondDialog registerSecondDialog(){
        return new RegisterSecondDialog(activity);
    }


    @RegisterScope
    @Provides
    public RegisterThirdDialog registerThirdDialog(){
        return new RegisterThirdDialog(activity);
    }


    @RegisterScope
    @Provides
    public RegisterFourthDialog registerFourthDialog(){
        return new RegisterFourthDialog(activity);
    }


    @RegisterScope
    @Provides
    public RegisterFifthDialog registerFifthDialog(){
        return new RegisterFifthDialog(activity);
    }
}
